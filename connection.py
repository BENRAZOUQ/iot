import asyncio
from bleak import BleakClient, BleakScanner

async def discover_and_connect(address):
    try:
        devices = await BleakScanner.discover()
        for device in devices:
            if device.address == address:
                async with BleakClient(device.address) as client:
                    if client.is_connected:
                        print("Connected to device:", device)
                        services = await client.get_services()
                        print("Services disponibles sur OBU-6771: ")
                        for service in services:
                            print(service)
                            for charac in service.characteristics:
                                print(charac)
                        for uuid in device.metadata("uuids"):
                            model_number = await client.read_gatt_char(uuid)
                            print(model_number)
                        return
        print("Device not found.")
    except Exception as e:
        print(f"Error: {e}")

address = "E7:90:A2:C0:1A:23"
asyncio.run(discover_and_connect(address))
